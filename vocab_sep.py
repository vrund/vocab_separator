from docx import Document
from docx.shared import Inches

document = Document('Vocab.docx') 
definitions_doc = Document()
syn_doc = Document()
types = ['Noun', 'Verb', 'Adjective']

for para in document.paragraphs:
  # Iterating over each word in the paragraph
  for run in para.runs:
    # Bolded word is the word
    if run.bold and len(run.text) > 1:
      # Concatenate syn and def for the word by joining parts of the text
      syn = ''
      dfn = ''
      # Split to get the second part of the vocab word
      syn_def_list = para.text.split(' - ')
      if len(syn_def_list) > 1:
        # Splitting by comma of the part after '-'
        words = syn_def_list[1].split(',')
        # the first part is always the definition
        dfn = dfn + words[0] + ', '
        for word in words[1:]:
          # Checking for syns
          if len(word.split('/')) > 1:
            dash_splitted_words = word.split('/')
            # Iterate to check if its really a syn
            for w in dash_splitted_words:
              # syns normally have only 1 space at the most
              if len(w.split()) < 3:
                syn = syn + word + '/'
                break
              # if there are more spaces, it means the word is a def
              else:
                dfn = dfn + word + ', '
                break
          # checking for definition
          elif len(word.split()) > 1:
            dfn = dfn + word + ', '
      if len(dfn):
        p = definitions_doc.add_paragraph()
        runner = p.add_run(syn_def_list[0])
        runner.bold = True
        p.add_run(' - ' + dfn)
      if len(syn) > 1:
        p = syn_doc.add_paragraph()
        runner = p.add_run(run.text)
        runner.bold = True
        p.add_run(' - ' + syn)
    elif run.text in types:
      definitions_doc.add_paragraph(run.text)
      syn_doc.add_paragraph(run.text)

definitions_doc.save('Definitions.docx')
syn_doc.save('Synonyms.docx')